package ru.romanow.protocols.soap.web;

/**
 * Created by ronin on 18.09.16
 */
public interface WebServiceClient {
    void makeRequest();
}
